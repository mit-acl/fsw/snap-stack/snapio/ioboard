ioboard
=======

Firmware for snap-stack io board, allowing the snap-stack to communicate with hardware (e.g., motors, GPIO, I2C, SPI, UART, etc).

## How to flash flight core?

### Requirements

Debugger cable (from USB programmer to VOXL Flight / Flight Core). See [this wiki page](https://gitlab.com/mit-acl/fsw/snap-stack/snapio/ioboard/-/wikis/How-to-make-a-debugger-cable-for-VOXL-Flight) for how to create the cable.


### Procedure

(The first two steps are from [this wiki page](https://github.com/plusk01/airdamon_f3/wiki#setup-arm-toolchain).)

1. On your computer (not VOXL), install the latest ARM toolchain from [this link](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads). Next we assume that `gcc-arm-none-eabi-10.3-2021.07-x86_64-linux.tar.bz2` has been downloaded (please replace with other versions appropriately). In the download folder, do:
  ```bash
  tar jxvf gcc-arm-none-eabi-10.3-2021.07-x86_64-linux.tar.bz2                        # replace with other versions 
  sudo mv gcc-arm-none-eabi-10.3-2021.07 /opt/                                        # replace with other versions
  echo 'export PATH="/opt/gcc-arm-none-eabi-10.3-2021.07/bin:$PATH"' >> ~/.bashrc     # replace with other versions
  ```
2. Install `openocd` by doing `sudo apt install openocd`.
3. The following code block clones the `ioboard` repo, checks out the `flight-core` branch (which will be merged to `main` in the future), properly initializes all the submodules, and finally builds the repo:
  ```bash
  git clone https://gitlab.com/mit-acl/fsw/snap-stack/snapio/ioboard
  git checkout flight-core
  git submodule update --init --recursive
  mkdir build && cd build && cmake ..
  ``` 
#### Troubleshooting for Ubuntu 18.04 (unnecessary for 20.04)

The default CMake version for Ubuntu 18.04 is below the minimum requirement of 3.15 to build `ioboard` repo, so cmake has to be upgraded. Beware that some answers online require removing the current cmake, which will cause partial deletion of ROS distribution. Please follow this solution here [on ROS Forum](https://answers.ros.org/question/293119/how-can-i-updateremove-cmake-without-partially-deleting-my-ros-distribution/). Essentially, download the latest release of source distributions from the [CMake website](https://cmake.org/download/) (e.g., `cmake-3.21.1.tar.gz` or other versions).
  ```bash
  tar -zxvf cmake-3.21.1.tar.gz
  cd cmake-3.21.1
  ./bootstrap --prefix=$HOME/cmake-install
  make 
  make install
  ```
Next, put these lines into your `~/.bashrc` or `~/.zshrc`:
  ```bash
  export PATH=$HOME/cmake-install/bin:$PATH
  export CMAKE_PREFIX_PATH=$HOME/cmake-install:$CMAKE_PREFIX_PATH
  ```

4. Flash the Flight Core: `make firmware-stlink-flash`
  *Make sure the debugger cable is connected properly: the USB programmer is connected to your computer and the JST end is plugged into the Flight Core.*
  ***Troubleshooting**: USB 3 connection doesn’t seem to work for the USB programmer. Try different USB ports on your computer if flashing is not successful.*

5. Lastly, verify that the green light on the Flight Core is on.
