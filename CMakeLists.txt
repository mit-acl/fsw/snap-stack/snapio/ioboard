cmake_minimum_required(VERSION 3.15 FATAL_ERROR)
set(CMAKE_TOOLCHAIN_FILE cmake/toolchain-arm-none-eabi-gcc.cmake)
project(ioboard VERSION 0.1.0 LANGUAGES CXX C ASM)

include(cmake/stm32f765.cmake)

# build ioboard library
add_subdirectory(src)

# build for specific targets
add_subdirectory(hal)
