/**
 * @file board.h
 * @brief Implementation of ioboard hal for a specific MCU/PCB
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#pragma once

#include <ioboard/hal.h>
#include "driver/system.h"

namespace acl {
namespace ioboard {
namespace hal {

  class FlightCore : public AbstractBoard
  {
  public:
    FlightCore();

    void init() override {}
    void reset(bool bootloader) override {}

    uint32_t clock_millis() override;
    uint32_t clock_micros() override;

    void serial_write(const uint8_t *src, size_t len) override;
    uint16_t serial_bytes_available() override;
    uint8_t serial_read() override;
    void serial_flush() override;

    void esc_write(size_t channel, float value) override;

    void rgb1_set_color(Color color, uint8_t brightness=100) override;
    void rgb2_set_color(Color color, uint8_t brightness=100) override;
    
  private:
    static constexpr size_t NUM_ESCS = 8;

    driver::UART uart_;
    std::array<driver::ESC, NUM_ESCS> escs_;
    driver::RGBLED rgb1_, rgb2_;

    void rgb_set_color(driver::RGBLED& rgb, Color color, uint8_t brightness);
  };

} // ns hal
} // ns ioboard
} // ns acl
