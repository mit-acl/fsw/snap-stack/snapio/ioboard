/**
 * @file main.cpp
 * @brief Entry point for ioboard using the Flight Core board
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#include <ioboard/ioboard.h>
#include "board.h"

int main()
{
  acl::ioboard::hal::FlightCore fc;
  acl::ioboard::IOBoard ioboard(fc);

  while (1) {
    ioboard.run();
  }

  return 0;
}