Synchronized Timer Perhipherals
===============================

Two independent timers can be synchronized. One is configured as a master (MMS bits in CR2), the other(s) as slave(s) (SMS bits in SMCR). See FIgure 296 in the [STM32F76xxx reference manual](https://www.st.com/resource/en/reference_manual/dm00224583-stm32f76xxx-and-stm32f77xxx-advanced-armbased-32bit-mcus-stmicroelectronics.pdf).

Put the slave in Reset Mode. Use the TS bits in SMCR to select the trigger to be the ITRx (internal trigger) corresponding to the master timer (see Table 177 in ref man).

It seemed that many people recommended the master mode be ENABLE - thus, TRGO (trigger output) would be set when CNT_EN for the master timer was set high (i.e., the master timer was enabled with `timer_enable_counter`). However, I noticed that (1) it was important to enable the master timer second (so that the slave timer would be reset), and (2) there seemed to be about 0.2 - 0.5 usec of delay between master timer and slave timer pwm starts. So I changed the master TRGO to be based on UPDATE -- "the update event is selected as TRGO". The update event seems to be the counter overflow (i.e., the rising edge of a new PWM signal). With this method, the start edges of the two sets of PWM occur within 0.05 usec (using the knock off saleae digital oscope).