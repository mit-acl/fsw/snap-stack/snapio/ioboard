/**
 * @file main.cpp
 * @brief Synchronized timers for PWM example
 * @author Parker Lusk <plusk@mit.edu>
 * @date 20 November 2021
 */

#include <array>

#include "driver/system.h"
#include "driver/esc.h"

using namespace acl::ioboard::hal;

constexpr int NUM_ESC_PINS = 8;
constexpr int PWM_FREQ_HZ = 490;
const driver::ESCConfig esc_configs[NUM_ESC_PINS] = {
  // timer, gpio_af, port, pin, timch, pwm_freq_hz
  {TIM1, GPIO_AF1, GPIOE, GPIO14, TIM_OC4, 0xff, PWM_FREQ_HZ},
  {TIM1, GPIO_AF1, GPIOA, GPIO10, TIM_OC3, 0xff, PWM_FREQ_HZ},
  {TIM1, GPIO_AF1, GPIOE, GPIO11, TIM_OC2, 0xff, PWM_FREQ_HZ},
  {TIM1, GPIO_AF1, GPIOA, GPIO8 , TIM_OC1, 0xff, PWM_FREQ_HZ},
  {TIM4, GPIO_AF2, GPIOD, GPIO13, TIM_OC2, TIM_SMCR_TS_ITR0, PWM_FREQ_HZ},
  {TIM4, GPIO_AF2, GPIOD, GPIO14, TIM_OC3, TIM_SMCR_TS_ITR0, PWM_FREQ_HZ},
  {TIM4, GPIO_AF2, GPIOD, GPIO12, TIM_OC1, TIM_SMCR_TS_ITR0, PWM_FREQ_HZ},
  {TIM4, GPIO_AF2, GPIOD, GPIO15, TIM_OC4, TIM_SMCR_TS_ITR0, PWM_FREQ_HZ},
};

int main()
{

  system_init();

  std::array<driver::ESC, NUM_ESC_PINS> escs;

  escs[0].init(&esc_configs[0]);
  escs[1].init(&esc_configs[1]);
  escs[2].init(&esc_configs[2]);
  escs[3].init(&esc_configs[3]);
  // timer_enable_counter(TIM1);

  escs[4].init(&esc_configs[4]);
  escs[5].init(&esc_configs[5]);
  // timer_enable_counter(TIM4);

  while (true) {

    escs[0].write(0.0);
    escs[1].write(0.2);
    escs[2].write(0.4);
    escs[3].write(0.6);
    escs[4].write(0.8);
    escs[5].write(1.0);

    uint32_t start_ms = millis();
    while (true) {
      if (millis() - start_ms >= 1000) break;
    }

    escs[0].write(0.8);
    escs[1].write(0.6);
    escs[2].write(0.4);
    escs[3].write(0.2);
    escs[4].write(0.0);
    escs[5].write(0.7);

    start_ms = millis();
    while (true) {
      if (millis() - start_ms >= 1000) break;
    }
  }

  return 0;
}