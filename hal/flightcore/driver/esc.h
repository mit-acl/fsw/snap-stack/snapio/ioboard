/**
 * @file esc.h
 * @brief Electronic speed control driver
 * @author Parker Lusk <plusk@mit.edu>
 * @date 19 July 2021
 */

#ifndef ESC_H_
#define ESC_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#ifdef __cplusplus
}
#endif

namespace acl {
namespace ioboard {
namespace hal {
namespace driver {

  struct ESCConfig {
    uint32_t timer;
    uint8_t gpio_af;

    // output pin
    uint32_t port;
    uint16_t pin;
    tim_oc_id timch;

    // timer synchronization
    uint8_t master_timer_ITR;

    // pwm params
    uint32_t pwm_freq_hz;
  };

  class ESC
  {
  public:
    void init(const ESCConfig* config);

    void write(float value);

  private:
    // pwm specific
    static constexpr int MIN_USEC = 1000;
    static constexpr int MAX_USEC = 2000;

    // low-level hw configuration for this object
    const ESCConfig* cfg_;

    // Initializers for low-level perhipherals and components
    void init_GPIO();
    void init_TIM();
  };

} // ns driver
} // ns hal
} // ns ioboard
} // ns acl

#endif // ESC_H_
