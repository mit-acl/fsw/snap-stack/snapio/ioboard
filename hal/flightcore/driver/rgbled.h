/**
 * @file rgbled.h
 * @brief RGB LED driver using timers for PWM-controlled brightness
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#ifndef RGBLED_H_
#define RGBLED_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#ifdef __cplusplus
}
#endif

namespace acl {
namespace ioboard {
namespace hal {
namespace driver {

  struct LEDConfig {
    // assumption: RGB gpios use the same AF - same TIM w/ diff channels
    uint32_t timer;
    uint8_t gpio_af;

    // Red LED
    uint32_t r_port;
    uint16_t r_pin;
    tim_oc_id r_timch;
    // Green LED
    uint32_t g_port;
    uint16_t g_pin;
    tim_oc_id g_timch;
    // Blue LED
    uint32_t b_port;
    uint16_t b_pin;
    tim_oc_id b_timch;

    bool active_low;
  };

  class RGBLED
  {
  public:
    enum class Color: uint8_t { OFF, RED, GREEN, BLUE, YELLOW, MAGENTA, CYAN, WHITE };
    void init(const LEDConfig* config);

    void off();

    void set_color(Color color, uint8_t brightness=100);

  private:
    // low-level hw configuration for this object
    const LEDConfig* cfg_;

    // Initializers for low-level perhipherals and components
    void init_GPIO(uint32_t port, uint16_t pin, uint8_t af);
    void init_TIM(uint32_t tim, tim_oc_id oc, uint16_t freq_hz, uint16_t steps);
  };

} // ns driver
} // ns hal
} // ns ioboard
} // ns acl

#endif // RGBLED_H_
