/**
 * @file rgbled.cpp
 * @brief RGB LED driver using timers for PWM-controlled brightness
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#include "driver/rgbled.h"

namespace acl {
namespace ioboard {
namespace hal {
namespace driver {

void RGBLED::init(const LEDConfig* config)
{
  cfg_ = config;

  static constexpr uint16_t PWM_FREQ_HZ = 100; // fast enough so not visible
  static constexpr uint16_t PWM_STEPS = 100; // enough resolution to map 1-1 with percent brightness

  init_GPIO(cfg_->r_port, cfg_->r_pin, cfg_->gpio_af);
  init_GPIO(cfg_->g_port, cfg_->g_pin, cfg_->gpio_af);
  init_GPIO(cfg_->b_port, cfg_->b_pin, cfg_->gpio_af);
  init_TIM(cfg_->timer, cfg_->r_timch, PWM_FREQ_HZ, PWM_STEPS);
  init_TIM(cfg_->timer, cfg_->g_timch, PWM_FREQ_HZ, PWM_STEPS);
  init_TIM(cfg_->timer, cfg_->b_timch, PWM_FREQ_HZ, PWM_STEPS);
}

// ----------------------------------------------------------------------------

void RGBLED::off()
{
  set_color(Color::OFF);
}

// ----------------------------------------------------------------------------

void RGBLED::set_color(Color color, uint8_t brightness)
{
  uint16_t r_oc = 0, g_oc = 0, b_oc = 0;

  if (brightness > 99) brightness = 99;

  switch (color) {
    case Color::RED:
      r_oc = brightness;
      g_oc = 0;
      b_oc = 0;
      break;
    case Color::GREEN:
      r_oc = 0;
      g_oc = brightness;
      b_oc = 0;
      break;
    case Color::BLUE:
      r_oc = 0;
      g_oc = 0;
      b_oc = brightness;
      break;
    case Color::YELLOW:
      r_oc = brightness;
      g_oc = brightness;
      b_oc = 0;
      break;
    case Color::MAGENTA:
      r_oc = brightness;
      g_oc = 0;
      b_oc = brightness;
      break;
    case Color::CYAN:
      r_oc = 0;
      g_oc = brightness;
      b_oc = brightness;
      break;
    case Color::WHITE:
      r_oc = brightness;
      g_oc = brightness;
      b_oc = brightness;
      break;
    case Color::OFF:
    default:
      break;
  }

  timer_set_oc_value(cfg_->timer, cfg_->r_timch, r_oc);
  timer_set_oc_value(cfg_->timer, cfg_->g_timch, g_oc);
  timer_set_oc_value(cfg_->timer, cfg_->b_timch, b_oc);
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void RGBLED::init_GPIO(uint32_t port, uint16_t pin, uint8_t af)
{
  gpio_mode_setup(port, GPIO_MODE_AF, GPIO_PUPD_NONE, pin);
  gpio_set_output_options(port, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, pin);
  gpio_set_af(port, af, pin);
}

// ----------------------------------------------------------------------------

void RGBLED::init_TIM(uint32_t tim, tim_oc_id oc, uint16_t freq_hz, uint16_t steps)
{
  //see http://www.micromouseonline.com/2016/02/06/pwm-basics-on-the-stm32-general-purpose-timers/

  // n.b., the 2x is due to APB1 DIV (ppre1) - see clock tree in ref man and clock config
  timer_set_prescaler(tim, ((rcc_apb1_frequency * 2) / (freq_hz * steps)) - 1);
  timer_set_mode(tim, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  timer_disable_preload(tim);
  timer_continuous_mode(tim);
  timer_set_period(tim, steps - 1);

  timer_set_oc_value(tim, oc, 0); // turn off (0% duty cycle)

  timer_set_oc_mode(tim, oc, TIM_OCM_PWM1);

  if (cfg_->active_low)
    timer_set_oc_polarity_low(tim, oc);

  timer_enable_oc_output(tim, oc);

  timer_enable_counter(tim);
}

} // ns driver
} // ns hal
} // ns ioboard
} // ns acl
