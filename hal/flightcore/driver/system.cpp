/**
 * @file system.cpp
 * @brief System-level configuration
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#include "driver/system.h"

/**
 * The clock configuratin we will use - 168 MHz AHB freq
 */
static const rcc_clock_scale* clock_config = &rcc_3v3[RCC_CLOCK_3V3_168MHZ];

/**
 * monotonically increasing number of microseconds from reset
 */
static volatile uint32_t system_micros = 0;

// ============================================================================

/**
 * @brief      Setup systick timer to create 1 usec ticks
 */
static void systick_init(void)
{
  static constexpr int tick_freq_hz = 1000000; // 1 usec
  systick_set_frequency(tick_freq_hz, clock_config->ahb_frequency);
  systick_counter_enable();
  nvic_set_priority(NVIC_SYSTICK_IRQ, 0); // highest priority interrupt
  systick_interrupt_enable();
}

// ----------------------------------------------------------------------------

/**
 * @brief      Clock setup. Take care that periphs in use have enabled clocks.
 */
static void clock_setup()
{

  // xtal osc in is 16 MHz
  // cf., https://docs.modalai.com/flight-core-datasheet-functional-description/#block-diagram
  static constexpr uint32_t hse_mhz = 16;
  rcc_clock_setup_hse(clock_config, hse_mhz);

  /* Enable GPIOD clock for LED & USARTs. */
  // rcc_periph_clock_enable(RCC_GPIOD);
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);
  // rcc_periph_clock_enable(RCC_GPIOC);
  rcc_periph_clock_enable(RCC_GPIOD);
  rcc_periph_clock_enable(RCC_GPIOE);
  // rcc_periph_clock_enable(RCC_GPIOF);
  // rcc_periph_clock_enable(RCC_GPIOG);
  rcc_periph_clock_enable(RCC_GPIOH);
  rcc_periph_clock_enable(RCC_GPIOI);

  rcc_periph_clock_enable(RCC_UART5);

  rcc_periph_clock_enable(RCC_DMA1);
  // rcc_periph_clock_enable(RCC_DMA2);

  // PWM timers
  rcc_periph_clock_enable(RCC_TIM1);
  rcc_periph_reset_pulse(RST_TIM1);
  rcc_periph_clock_enable(RCC_TIM4);
  rcc_periph_reset_pulse(RST_TIM4);

  // RGB1 timer
  rcc_periph_clock_enable(RCC_TIM3);
  rcc_periph_reset_pulse(RST_TIM3);
  // RGB2 timer
  rcc_periph_clock_enable(RCC_TIM5);
  rcc_periph_reset_pulse(RST_TIM5);
}

// ============================================================================

#define TIM_PSC_DIV 48000
#define SECONDS     1

void system_init()
{
  clock_setup();
  systick_init();
}

// ----------------------------------------------------------------------------

uint32_t micros()
{
  return system_micros;
}

// ----------------------------------------------------------------------------

uint32_t millis()
{
  return system_micros / 1000;
}

// ============================================================================

/**
 * @brief      IRQ for systick
 */
extern "C" void sys_tick_handler(void)
{
    system_micros++;
}