/**
 * @file esc.h
 * @brief Electronic speed control driver
 * @author Parker Lusk <plusk@mit.edu>
 * @date 19 July 2021
 */

#include "driver/esc.h"

namespace acl {
namespace ioboard {
namespace hal {
namespace driver {

void ESC::init(const ESCConfig* config)
{
  cfg_ = config;

  init_GPIO();
  init_TIM();
}

// ----------------------------------------------------------------------------

void ESC::write(float value)
{
  if (value < 0) value = 0;
  if (value > 1) value = 1;
  uint16_t CCR = MIN_USEC + (MAX_USEC - MIN_USEC) * value;
  timer_set_oc_value(cfg_->timer, cfg_->timch, CCR);
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void ESC::init_GPIO()
{
  gpio_mode_setup(cfg_->port, GPIO_MODE_AF, GPIO_PUPD_NONE, cfg_->pin);
  gpio_set_output_options(cfg_->port, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, cfg_->pin);
  gpio_set_af(cfg_->port, cfg_->gpio_af, cfg_->pin);
}

// ----------------------------------------------------------------------------

void ESC::init_TIM()
{
  //see http://www.micromouseonline.com/2016/02/06/pwm-basics-on-the-stm32-general-purpose-timers/
  timer_disable_counter(cfg_->timer);
  timer_set_counter(cfg_->timer, 0);

  // see block diagram in data sheet
  const bool on_apb1 = (cfg_->timer == TIM2)
                    || (cfg_->timer == TIM3)
                    || (cfg_->timer == TIM4)
                    || (cfg_->timer == TIM5)
                    || (cfg_->timer == TIM6)
                    || (cfg_->timer == TIM7)
                    || (cfg_->timer == TIM12)
                    || (cfg_->timer == TIM13)
                    || (cfg_->timer == TIM14);

  // n.b., the 2x is due to APBx DIV (pprex) - see clock tree in ref man and libopencm3 rcc_clock_scale
  const uint32_t timfreq = 2 * ((on_apb1) ? rcc_apb1_frequency : rcc_apb2_frequency);

  const uint32_t cycle_time_us = 1e6 / cfg_->pwm_freq_hz;
  const uint32_t STEPS = cycle_time_us; // full microsecond resolution, i.e., 0-2000 usec signal
  const uint32_t cntfreq = cfg_->pwm_freq_hz * STEPS;
  // const uint32_t cntfreq = 100 * STEPS;

  timer_set_prescaler(cfg_->timer, (timfreq / cntfreq) - 1);
  timer_set_mode(cfg_->timer, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  timer_disable_preload(cfg_->timer);
  timer_continuous_mode(cfg_->timer);
  timer_set_period(cfg_->timer, STEPS - 1);

  // advanced timers must enable main output
  // https://community.st.com/s/question/0D50X00009XkYbm/having-problems-with-stm32f7-tim1-pwm-mode-1-output-pins
  const bool advtim = (cfg_->timer == TIM1)
                   || (cfg_->timer == TIM8);
  if (advtim)
    timer_enable_break_main_output(cfg_->timer);

  if (cfg_->master_timer_ITR != 0xff) {
    // see Table 177 of RF
    timer_slave_set_trigger(cfg_->timer, cfg_->master_timer_ITR);
    timer_slave_set_mode(cfg_->timer, TIM_SMCR_SMS_RM);
  }

  if (cfg_->master_timer_ITR == 0xff) {
    timer_set_master_mode(cfg_->timer, TIM_CR2_MMS_UPDATE);
  }

  timer_set_oc_value(cfg_->timer, cfg_->timch, MIN_USEC); // turn off (0% duty cycle)

  timer_set_oc_mode(cfg_->timer, cfg_->timch, TIM_OCM_PWM1);

  timer_enable_oc_output(cfg_->timer, cfg_->timch);

  timer_enable_counter(cfg_->timer);
}

} // ns driver
} // ns hal
} // ns ioboard
} // ns acl
