/**
 * @file uart.h
 * @brief UART driver - uses circular buffer + DMA for Rx and Tx
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 * cf., https://github.com/plusk01/airdamon_f3/blob/master/chips/stm32f30xC/src/uart.cpp
 */

#ifndef UART_H_
#define UART_H_

#include <functional>

#ifdef __cplusplus
extern "C" {
#endif
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>
#ifdef __cplusplus
}
#endif

namespace acl {
namespace ioboard {
namespace hal {
namespace driver {

  /**
   * @brief      Configuration of peripherals for UART driver.
   * 
   * n.b., although this abstracts out the hardware associated with
   * the UART peripheral (e.g., USART, GPIO, DMA, IRQs, etc.) - there
   * is still an element of "hardcoding" in the cpp file due to IRQ
   * handling. For each potential USART/DMA configuration, the
   * corresponding IRQ should already be coded in the cpp file.
   */
  struct UARTConfig {
    // The relevant USART peripheral
    uint32_t USARTx;
    
    // USART GPIO ports, pins, and alternate functions for RX and TX
    uint32_t rx_port;
    uint16_t rx_pin;
    uint8_t rx_af;

    uint32_t tx_port;
    uint16_t tx_pin;
    uint8_t tx_af;

    // DMA controller for usart rx and tx
    uint32_t Rx_DMA, Tx_DMA;

    // DMA streams for usart rx and tx
    uint8_t Rx_DMA_STREAM, Tx_DMA_STREAM;

    // DMA Channels regs for USART Rx and Tx
    uint32_t Rx_DMA_Channel, Tx_DMA_Channel;

    // USART global interrupt IRQ number
    uint8_t USARTx_IRQn;

    // USART Tx Channel DMA IRQ number
    uint8_t Tx_DMA_IRQn;
  };

  /**
   * @brief      UART driver
   * 
   * C++ object that allows use of a USART with DMA capabilities.
   * USART with polling or interrupts not supported by the class,
   * though could easily be added.
   *
   * There is no logic preventing calling code to overwrite data
   * in the Tx buffer. It is up to the caller to check the size
   * of the data to write with the TX_BUFFER_SIZE and then split
   * the data up accordingly, (see e.g., would_stomp_dma_data).
   */
  class UART
  {
  private:
    /**
     * Sizes for circular buffers that interface with user and DMA.
     * Increase size if bytes are being dropped...
     */
    static constexpr int RX_BUFFER_SIZE = 64;
    static constexpr int TX_BUFFER_SIZE = 64;
  public:
    enum class Mode: uint8_t { m8N1, m8E2 };

    /**
     * @brief      Initialize a USART/UART peripheral
     *
     * @param[in]  config    Pre-determined configuration for desired USART/UART
     * @param[in]  baudrate  Serial baudrate
     * @param[in]  mode      e.g., 8 bits, no parity, 1 stop bit is 8N1
     */
    void init(const UARTConfig* config,
              uint32_t baudrate=115200, Mode mode = Mode::m8N1);

    /**
     * @brief      Change the baurdate and mode of USART/UART
     *
     * @param[in]  baudrate  Serial baudrate
     * @param[in]  mode      e.g., 8 bits, no parity, 1 stop bit is 8N1
     */
    void set_mode(uint32_t baudrate, Mode mode = Mode::m8N1);

    /**
     * Rx Functions
     */

    /**
     * @brief      Check to see how many new bytes are in received buffer
     *
     * @return     Number of bytes to read
     */
    uint16_t rx_bytes_waiting();

    /**
     * @brief      Read a single byte from received buffer.
     *             
     *             Precondition: Must have already called `rx_bytes_waiting()`.
     *
     * @return     Byte in received buffer
     */
    uint8_t read_byte();


    /**
     * Tx Functions
     */

    /**
     * @brief      Transmit a byte array via USARTx
     *
     * @param      buf   buffer
     * @param[in]  len   length
     */
    void write(const uint8_t* buf, uint8_t len);

    /**
     * @brief      Waits for any pending transmissions to complete.
     *             See also Arduino Serial.flush
     */

    /**
     * @brief      Waits for any pending transmissions to complete.
     *             See also Arduino Serial.flush
     *
     * @return     False if wait timed out (i.e., serial didn't flush)
     */
    bool flush();

    // Would adding another byte of data stomp on data that
    // the DMA could possibly still need for a transfer?
    bool would_stomp_dma_data();

  private:
    // low-level hw configuration for this UART object
    const UARTConfig* cfg_;

    // Buffers to hold data managed by DMA, from/to Rx/Tx
    uint8_t rx_buffer_[RX_BUFFER_SIZE];
    uint8_t tx_buffer_[TX_BUFFER_SIZE];
    uint16_t rx_buffer_head_, rx_buffer_tail_;
    uint16_t tx_buffer_head_, tx_buffer_tail_;
    uint16_t tx_old_DMA_pos_;

    // Initializers for low-level perhipherals and components
    void init_GPIO();
    void init_UART(uint32_t baudrate, Mode mode = Mode::m8N1);
    void init_DMA();
    void init_NVIC();

  /// \brief Public because called from IRQs
  public:
    // Start a DMA transfer from buffer to Tx reg of USARTx.
    // The only reason this is public is for access from the IRQ handler.
    void start_DMA_transfer();

    // Is there any data in the buffer that needs to be sent?
    bool tx_buffer_empty();

  };

} // ns driver
} // ns hal
} // ns ioboard
} // ns acl

#endif // UART_H_
