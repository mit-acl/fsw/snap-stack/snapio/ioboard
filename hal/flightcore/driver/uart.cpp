/**
 * @file uart.cpp
 * @brief UART driver - uses circular buffer + DMA for Rx and Tx
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 * cf., https://github.com/plusk01/airdamon_f3/blob/master/chips/stm32f30xC/src/uart.cpp
 */

#include "driver/uart.h"

// ============================================================================

// pointers to UART objects for use in hardcoded IRQ handlers
static acl::ioboard::hal::driver::UART* UART1Ptr = nullptr;
static acl::ioboard::hal::driver::UART* UART5Ptr = nullptr;

// ============================================================================

namespace acl {
namespace ioboard {
namespace hal {
namespace driver {

void UART::init(const UARTConfig* config, uint32_t baudrate, Mode mode)
{
  cfg_ = config;

  // Store the pointer to this object for use in hardcoded IRQ handlers
  if (cfg_->USARTx == USART1) UART1Ptr = this;
  else if (cfg_->USARTx == UART5) UART5Ptr = this;

  init_GPIO();
  init_DMA();
  init_UART(baudrate, mode);
  init_NVIC();
}

// ----------------------------------------------------------------------------

void UART::set_mode(uint32_t baudrate, Mode mode)
{
 init_UART(baudrate, mode);
}

// ----------------------------------------------------------------------------

void UART::write(const uint8_t* buf, uint8_t len)
{
  // Put data into the tx_buffer
  for (uint8_t i=0; i<len; i++)
  {
    tx_buffer_[tx_buffer_head_] = buf[i];

    // Move the head of the buffer, wrapping around if necessary
    tx_buffer_head_ = (tx_buffer_head_ + 1) % TX_BUFFER_SIZE;
  }

  // Ensure that the DMA transfer is finished (by making sure the channel
  // is disabled) before trying to initiate a new transfer
  const bool isDMAStreamEnabled = DMA_SCR(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM) & DMA_SxCR_EN;
  if (!isDMAStreamEnabled) {
    start_DMA_transfer();
  }
}

// ----------------------------------------------------------------------------

uint8_t UART::read_byte()
{
  uint8_t byte = 0;

  // pull the next byte off the Rx buffer
  // (the head counts down, because NDTR counts down)
  if (rx_buffer_head_ != rx_buffer_tail_)
  {
    // read a new byte and decrement the tail
    byte = rx_buffer_[RX_BUFFER_SIZE - rx_buffer_tail_];

    // if at the bottom of buffer, wrap to the top
    if (--rx_buffer_tail_ == 0)
      rx_buffer_tail_ = RX_BUFFER_SIZE;
  }

  return byte;
}

// ----------------------------------------------------------------------------

uint16_t UART::rx_bytes_waiting()
{
  uint16_t num_waiting_bytes = 0;

  // Get the current buffer head -- remember, DMA CNDTR counts down
  rx_buffer_head_ = dma_get_number_of_data(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM);

  // if pointing at the same location, there are no new bytes
  if (rx_buffer_head_ == rx_buffer_tail_)
    num_waiting_bytes = 0;

  // There is contiguous data (i.e., has not wrapped to top yet)
  else if (rx_buffer_head_ < rx_buffer_tail_)
    num_waiting_bytes = rx_buffer_tail_ - rx_buffer_head_;

  // There was a wrap around, so not contiguous -- count parts on either end.
  else if (rx_buffer_head_ > rx_buffer_tail_)
    num_waiting_bytes = RX_BUFFER_SIZE - rx_buffer_head_ + rx_buffer_tail_ + 1;

  return num_waiting_bytes;
}

// ----------------------------------------------------------------------------

bool UART::flush()
{
  uint32_t timeout = 100000;
  while (!tx_buffer_empty() && --timeout);
  return (timeout != 0); // didn't timeout, so flush was succesful
}

// ----------------------------------------------------------------------------

bool UART::would_stomp_dma_data()
{
  const bool isDMAStreamEnabled = DMA_SCR(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM) & DMA_SxCR_EN;
  const bool would_stomp = (((tx_buffer_head_+1) % TX_BUFFER_SIZE) == tx_old_DMA_pos_);
  return isDMAStreamEnabled && would_stomp;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void UART::init_GPIO()
{
  // Configure GPIO and connect to UART RX
  gpio_mode_setup(cfg_->rx_port, GPIO_MODE_AF, GPIO_PUPD_NONE, cfg_->rx_pin);
  gpio_set_af(cfg_->rx_port, cfg_->rx_af, cfg_->rx_pin);

  // Configure GPIO and connect to UART TX
  gpio_mode_setup(cfg_->tx_port, GPIO_MODE_AF, GPIO_PUPD_NONE, cfg_->tx_pin);
  gpio_set_af(cfg_->tx_port, cfg_->tx_af, cfg_->tx_pin);
}

// ----------------------------------------------------------------------------

void UART::init_UART(uint32_t baudrate, Mode mode)
{

  // Configure a bi-directional, UART device
  usart_set_baudrate(cfg_->USARTx, baudrate);
  usart_set_mode(cfg_->USARTx, USART_MODE_TX_RX);
  usart_set_flow_control(cfg_->USARTx, USART_FLOWCONTROL_NONE);

  if (mode == Mode::m8N1)
  {
    usart_set_databits(cfg_->USARTx, 8);
    usart_set_stopbits(cfg_->USARTx, USART_STOPBITS_1);
    usart_set_parity(cfg_->USARTx, USART_PARITY_NONE);
  }
  else if (mode == Mode::m8E2)
  {
    // 9 bit UART word because of the parity bit
    usart_set_databits(cfg_->USARTx, 9);
    usart_set_stopbits(cfg_->USARTx, USART_STOPBITS_2);
    usart_set_parity(cfg_->USARTx, USART_PARITY_EVEN);
  }

  // Enable interrupts on "receive buffer not empty" (i.e., byte received)
  // usart_enable_rx_interrupt(cfg_->USARTx);

  usart_enable(cfg_->USARTx);
}

// ----------------------------------------------------------------------------

void UART::init_DMA()
{
  //
  // USART Tx DMA Config (memory to peripheral)
  //

  dma_stream_reset(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM);
  dma_set_peripheral_address(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM, reinterpret_cast<uint32_t>(&USART_TDR(cfg_->USARTx)));
  // dma_set_memory_address(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM, reinterpret_cast<uint32_t>(tx_buffer_));
  // dma_set_number_of_data(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM, TX_BUFFER_SIZE);
  dma_set_transfer_mode(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM, DMA_SxCR_DIR_MEM_TO_PERIPHERAL);
  dma_set_peripheral_size(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM, DMA_SxCR_PSIZE_8BIT);
  dma_set_memory_size(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM, DMA_SxCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM);
  dma_set_priority(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM, DMA_SxCR_PL_HIGH);
  dma_channel_select(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM, cfg_->Tx_DMA_Channel);

  //
  // USART Rx DMA Config (peripheral to memory)
  //

  dma_stream_reset(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM);
  dma_set_peripheral_address(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM, reinterpret_cast<uint32_t>(&USART_RDR(cfg_->USARTx)));
  dma_set_memory_address(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM, reinterpret_cast<uint32_t>(rx_buffer_));
  dma_set_number_of_data(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM, RX_BUFFER_SIZE);
  dma_set_transfer_mode(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM, DMA_SxCR_DIR_PERIPHERAL_TO_MEM);
  dma_set_peripheral_size(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM, DMA_SxCR_PSIZE_8BIT);
  dma_set_memory_size(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM, DMA_SxCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM);
  dma_enable_circular_mode(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM);
  dma_set_priority(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM, DMA_SxCR_PL_HIGH);
  dma_channel_select(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM, cfg_->Rx_DMA_Channel);

  //
  // Enable perhipherals, manage interrupts, initialize buffers, etc
  //

  //  Hook up the DMA to the uart
  usart_enable_tx_dma(cfg_->USARTx);
  usart_enable_rx_dma(cfg_->USARTx);

  // Turn on the 'transfer complete' interrupt from the Tx DMA
  dma_enable_transfer_complete_interrupt(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM);

  // Initialize the Rx/Tx buffers
  rx_buffer_head_ = RX_BUFFER_SIZE; // DMA counts down on receive
  rx_buffer_tail_ = RX_BUFFER_SIZE;
  tx_buffer_head_ = 0;
  tx_buffer_tail_ = 0;
  tx_old_DMA_pos_ = 0;

  // Turn on the Rx DMA, since it is in circular mode
  dma_enable_stream(cfg_->Rx_DMA, cfg_->Rx_DMA_STREAM);
}

// ----------------------------------------------------------------------------

void UART::init_NVIC()
{
  // Configure the Nested Vector Interrupt Controller

  // DMA Tx Channel Interrupt
  nvic_enable_irq(cfg_->Tx_DMA_IRQn);


  // USART Global Interrupt
  nvic_enable_irq(cfg_->USARTx_IRQn);
}

// ----------------------------------------------------------------------------
// "Private" methods that are public due to C IQR handler
// ----------------------------------------------------------------------------

void UART::start_DMA_transfer()
{
  // Set the start of the transmission to the oldest data
  dma_set_memory_address(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM,
                    reinterpret_cast<uint32_t>(&tx_buffer_[tx_buffer_tail_]));

  // save the last known DMA position (for checking data stomping)
  tx_old_DMA_pos_ = tx_buffer_tail_;

  // Check to see if the data in the buffer is contiguous or not
  // (i.e., has it wrapped around the end of the buffer?)
  if (tx_buffer_head_ > tx_buffer_tail_) {

    // Set the length of the transmission to the data on the buffer.
    // If contiguous, this is easy.
    dma_set_number_of_data(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM,
                    tx_buffer_head_ - tx_buffer_tail_);

    // This signifies that there is no new/usable data in the buffer
    tx_buffer_tail_ = tx_buffer_head_;

  } else {

    // We will have to send the data in two groups, first the tail,
    // then the head we will do later
    dma_set_number_of_data(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM,
                    TX_BUFFER_SIZE - tx_buffer_tail_);

    // Set the tail to the beginning so that next time the leftover
    // data (which is now contiguous) will be sent.
    tx_buffer_tail_ = 0;
  }

  // Start the transmission from the Tx buffer
  // to the Tx register in the USART peripheral
  dma_enable_stream(cfg_->Tx_DMA, cfg_->Tx_DMA_STREAM);
}

// ----------------------------------------------------------------------------

bool UART::tx_buffer_empty()
{
  return tx_buffer_head_ == tx_buffer_tail_;
}

} // ns driver
} // ns hal
} // ns ioboard
} // ns acl


// ----------------------------------------------------------------------------
// Hardcoded IRQ handlers associated with USART/DMA
// ----------------------------------------------------------------------------

/**
 * @brief      UART5_TX - transfer complete interrupt
 */
extern "C" void dma1_stream7_isr(void)
{
  if (dma_get_interrupt_flag(DMA1, DMA_STREAM7, DMA_TCIF)) {
    dma_clear_interrupt_flags(DMA1, DMA_STREAM7, DMA_TCIF);

    // Signal that we are done with the latest DMA transfer
    dma_disable_stream(DMA1, DMA_STREAM7);

    // If there is still data to process, start again.
    // This happens when data was added to the buffer, but we were in
    // the middle of a transfer. Now that the transfer is finished
    // (marked by disabling the DMA), we can process the buffer.
    if (!UART5Ptr->tx_buffer_empty())
      UART5Ptr->start_DMA_transfer();
  }
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

/**
 * @brief      USART1_TX - transfer complete interrupt
 */
extern "C" void dma2_stream7_isr(void)
{
  if (dma_get_interrupt_flag(DMA2, DMA_STREAM7, DMA_TCIF)) {
    dma_clear_interrupt_flags(DMA2, DMA_STREAM7, DMA_TCIF);

    // Signal that we are done with the latest DMA transfer
    dma_disable_stream(DMA2, DMA_STREAM7);

    // If there is still data to process, start again.
    // This happens when data was added to the buffer, but we were in
    // the middle of a transfer. Now that the transfer is finished
    // (marked by disabling the DMA), we can process the buffer.
    if (!UART1Ptr->tx_buffer_empty())
      UART1Ptr->start_DMA_transfer();
  }
}