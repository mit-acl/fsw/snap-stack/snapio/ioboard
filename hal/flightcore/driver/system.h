/**
 * @file system.h
 * @brief System-level configuration
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <cstddef>

#ifdef __cplusplus
extern "C" {
#endif
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#ifdef __cplusplus
}
#endif

#include "driver/uart.h"
#include "driver/esc.h"
#include "driver/rgbled.h"

uint32_t micros();
uint32_t millis();

void system_init();

#endif // SYSTEM_H_