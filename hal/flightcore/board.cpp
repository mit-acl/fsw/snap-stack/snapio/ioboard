/**
 * @file board.cpp
 * @brief Implementation of ioboard hal for a specific MCU/PCB
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#include "board.h"

#define FLIGHTCOREV1

namespace acl {
namespace ioboard {
namespace hal {

///////////////////////////////////////////////////////////////////////////////
//                            UART Configuration                             //
///////////////////////////////////////////////////////////////////////////////

// !! Ensure that correct peripheral clocks are enabled !!

constexpr int NUM_UARTS = 2;
constexpr int CFG_UART1 = 0;
constexpr int CFG_UART5 = 1;
const driver::UARTConfig uart_configs[NUM_UARTS] = {
  // USARTx, rx_gpio, rx_pin, rx_af, tx_gpio, tx_pin, tx_af, Rx_DMA, Tx_DMA, Rx_DMA_STREAM, Tx_DMA_STREAM, Rx_DMA_Channel, Tx_DMA_Channel, USARTx_IRQn, Tx_DMA_IRQn
  // UART1 J10 // J1012
  {USART1, GPIOB, GPIO14, GPIO_AF4, GPIOB, GPIO15, GPIO_AF4, DMA2, DMA2, DMA_STREAM5, DMA_STREAM7, DMA_SxCR_CHSEL_4, DMA_SxCR_CHSEL_4, NVIC_USART1_IRQ, NVIC_DMA2_STREAM7_IRQ},
  // UART5 J1 // internally connected to VOXL
  {UART5, GPIOD, GPIO2, GPIO_AF8, GPIOB, GPIO9, GPIO_AF7, DMA1, DMA1, DMA_STREAM0, DMA_STREAM7, DMA_SxCR_CHSEL_4, DMA_SxCR_CHSEL_4, NVIC_UART5_IRQ, NVIC_DMA1_STREAM7_IRQ},
};

///////////////////////////////////////////////////////////////////////////////
//                          RGBLED Configuration                             //
///////////////////////////////////////////////////////////////////////////////

// !! Ensure that correct peripheral clocks are enabled !!

constexpr int NUM_RGBS = 2;
constexpr int CFG_RGB1 = 0;
constexpr int CFG_RGB2 = 1;
const driver::LEDConfig rgbled_configs[NUM_RGBS] = {
  // timer, gpio_af, r_port, r_pin, r_timch, g_port, g_pin, g_timch, b_port, b_pin, b_timch, active_low
  // RGB1
  {TIM3, GPIO_AF2, GPIOB, GPIO0, TIM_OC3, GPIOB, GPIO1, TIM_OC4, GPIOA, GPIO7, TIM_OC2, true},
  // RGB2
  {TIM5, GPIO_AF2, GPIOI, GPIO0, TIM_OC4, GPIOH, GPIO11, TIM_OC2, GPIOA, GPIO2, TIM_OC3, true},
};

///////////////////////////////////////////////////////////////////////////////
//                            ESC Configuration                              //
///////////////////////////////////////////////////////////////////////////////

// !! Ensure that correct peripheral clocks are enabled !!

constexpr int NUM_ESC_PINS = 8;
constexpr int PWM_FREQ_HZ = 490;
const driver::ESCConfig esc_configs[NUM_ESC_PINS] = {
  // timer, gpio_af, port, pin, timch, pwm_freq_hz
  {TIM1, GPIO_AF1, GPIOE, GPIO14, TIM_OC4, 0xff, PWM_FREQ_HZ},
  {TIM1, GPIO_AF1, GPIOA, GPIO10, TIM_OC3, 0xff, PWM_FREQ_HZ},
  {TIM1, GPIO_AF1, GPIOE, GPIO11, TIM_OC2, 0xff, PWM_FREQ_HZ},
  {TIM1, GPIO_AF1, GPIOA, GPIO8 , TIM_OC1, 0xff, PWM_FREQ_HZ},
  {TIM4, GPIO_AF2, GPIOD, GPIO13, TIM_OC2, TIM_SMCR_TS_ITR0, PWM_FREQ_HZ},
  {TIM4, GPIO_AF2, GPIOD, GPIO14, TIM_OC3, TIM_SMCR_TS_ITR0, PWM_FREQ_HZ},
  {TIM4, GPIO_AF2, GPIOD, GPIO12, TIM_OC1, TIM_SMCR_TS_ITR0, PWM_FREQ_HZ},
  {TIM4, GPIO_AF2, GPIOD, GPIO15, TIM_OC4, TIM_SMCR_TS_ITR0, PWM_FREQ_HZ},
};

// ============================================================================

FlightCore::FlightCore()
{
  system_init();

  uart_.init(&uart_configs[CFG_UART5], 921600);
  rgb1_.init(&rgbled_configs[CFG_RGB1]);
  rgb2_.init(&rgbled_configs[CFG_RGB2]);

  for (size_t i=0; i<escs_.size(); ++i) {
    escs_[i].init(&esc_configs[i]);
  }

}

// ----------------------------------------------------------------------------

uint32_t FlightCore::clock_millis()
{
  return millis();
}

// ----------------------------------------------------------------------------

uint32_t FlightCore::clock_micros()
{
  return micros();
}

// ----------------------------------------------------------------------------

void FlightCore::serial_write(const uint8_t *src, size_t len)
{
  uart_.write(src, len);
}

// ----------------------------------------------------------------------------

uint16_t FlightCore::serial_bytes_available()
{
  return uart_.rx_bytes_waiting();
}

// ----------------------------------------------------------------------------

uint8_t FlightCore::serial_read()
{
  return uart_.read_byte();
}

// ----------------------------------------------------------------------------

void FlightCore::serial_flush()
{
  uart_.flush();
}

// ----------------------------------------------------------------------------

void FlightCore::esc_write(size_t channel, float value)
{
  escs_[channel].write(value);
}

// ----------------------------------------------------------------------------

void FlightCore::rgb1_set_color(Color color, uint8_t brightness)
{
  rgb_set_color(rgb1_, color, brightness);
}

// ----------------------------------------------------------------------------

void FlightCore::rgb2_set_color(Color color, uint8_t brightness)
{
  rgb_set_color(rgb2_, color, brightness);
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void FlightCore::rgb_set_color(driver::RGBLED& rgb, Color color, uint8_t brightness)
{
  switch (color) {
    case Color::RED:
      rgb.set_color(driver::RGBLED::Color::RED, brightness);
      break;
    case Color::GREEN:
      rgb.set_color(driver::RGBLED::Color::GREEN, brightness);
      break;
    case Color::BLUE:
      rgb.set_color(driver::RGBLED::Color::BLUE, brightness);
      break;
    case Color::YELLOW:
      rgb.set_color(driver::RGBLED::Color::YELLOW, brightness);
      break;
    case Color::MAGENTA:
      rgb.set_color(driver::RGBLED::Color::MAGENTA, brightness);
      break;
    case Color::CYAN:
      rgb.set_color(driver::RGBLED::Color::CYAN, brightness);
      break;
    case Color::WHITE:
      rgb.set_color(driver::RGBLED::Color::WHITE, brightness);
      break;
    case Color::OFF:
    default:
      rgb.set_color(driver::RGBLED::Color::OFF, brightness);
      break;
  }
}

} // ns hal
} // ns ioboard
} // ns acl