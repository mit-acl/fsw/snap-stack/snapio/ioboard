/**
 * @file comm_manager.cpp
 * @brief Communication handling with flight computer
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#include "ioboard/comm_manager.h"
#include "ioboard/ioboard.h"

namespace acl {
namespace ioboard {

CommManager::CommManager(IOBoard& ioboard)
: ioboard_(ioboard), board_(ioboard.board_)
{
  streams_[STREAM_ID_HEARTBEAT].set_rate_hz(1);
}

// ----------------------------------------------------------------------------

void CommManager::receive()
{
  while (board_.serial_bytes_available()) {
    if (sio_serial_parse_byte(board_.serial_read(), &in_buf_)) {
      last_msg_time_us_ = board_.clock_micros();
      handle_sio_message();
    }
  }
}

// ----------------------------------------------------------------------------

void CommManager::stream()
{
  uint32_t time_us = board_.clock_micros();
  for (size_t i=0; i<STREAM_COUNT; i++)
  {
    streams_[i].stream(time_us);
  }
  board_.serial_flush();
}

// ----------------------------------------------------------------------------

bool CommManager::is_stale()
{
  return (board_.clock_micros() - last_msg_time_us_) > COMM_TIMEOUT_USEC;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void CommManager::handle_sio_message()
{
  switch (in_buf_.type)
  {
  case SIO_SERIAL_MSG_MOTORCMD:
    sio_serial_motorcmd_msg_t msg;
    sio_serial_motorcmd_msg_unpack(&msg, &in_buf_);
    handle_msg_motorcmd(msg);
    break;
  default:
    break;
  }
}

// ----------------------------------------------------------------------------
// Message Handlers
// ----------------------------------------------------------------------------

void CommManager::handle_msg_motorcmd(const sio_serial_motorcmd_msg_t& msg)
{
  ioboard_.motors_.set_new_motorcmd(msg.throttle);
}

// ----------------------------------------------------------------------------
// Message Senders
// ----------------------------------------------------------------------------

void CommManager::send_heartbeat()
{
  // make it easy to see that we are still alive
  static bool toggle = true;
  hal::Color color = (toggle) ? hal::Color::WHITE : hal::Color::OFF;
  static constexpr uint8_t brightness = 2;
  board_.rgb2_set_color(color, brightness);
  toggle = !toggle;
}

// ----------------------------------------------------------------------------
// CommManager::Stream
// ----------------------------------------------------------------------------

CommManager::Stream::Stream(uint32_t period_us, std::function<void(void)> send_function)
: period_us_(period_us), next_time_us_(0), send_function_(send_function)
{}

// ----------------------------------------------------------------------------

void CommManager::Stream::stream(uint32_t now_us)
{
  if (period_us_ > 0 && now_us >= next_time_us_) {
    // if you fall behind, skip messages
    do {
      next_time_us_ += period_us_;
    } while (next_time_us_ < now_us);

    send_function_();
  }
}

// ----------------------------------------------------------------------------

void CommManager::Stream::set_rate_hz(uint32_t rate_hz)
{
  period_us_ = (rate_hz == 0) ? 0 : 1000000 / rate_hz;
}

} // ns ioboard
} // ns acl