/**
 * @file motors.cpp
 * @brief Module for communicating with the motor ESCs
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#include "ioboard/motors.h"
#include "ioboard/ioboard.h"

namespace acl {
namespace ioboard {

Motors::Motors(IOBoard& ioboard)
: ioboard_(ioboard), board_(ioboard.board_)
{}

// ----------------------------------------------------------------------------

void Motors::set_new_motorcmd(const float throttle[NUM_MOTORS])
{
  memcpy(throttle_, throttle, NUM_MOTORS * sizeof(float));
  has_new_cmd_ = true;
}

// ----------------------------------------------------------------------------

void Motors::write()
{
  if (has_new_cmd_) {

    for (size_t i=0; i<NUM_MOTORS; ++i) {
      board_.esc_write(i, throttle_[i]);
    }

    has_new_cmd_ = false;
  }
}

// ----------------------------------------------------------------------------

void Motors::stop()
{
  for (size_t i=0; i<NUM_MOTORS; ++i) {
    board_.esc_write(i, 0.0);
  }
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------


} // ns ioboard
} // ns acl