/**
 * @file ioboard.cpp
 * @brief Main ioboard execution
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#include "ioboard/ioboard.h"

namespace acl {
namespace ioboard {

void IOBoard::run()
{

  // receives any messages sent from flight computer
  comm_manager_.receive();

  motors_.write();

  // sensors_.run(); // read any connected sensors...

  // send any relevant messages to flight computer
  comm_manager_.stream();

  // ========================

  // failsafe - if no recent message, stop motors
  static constexpr uint8_t brightness = 100;
  if (comm_manager_.is_stale()) {
    motors_.stop();
    board_.rgb1_set_color(hal::Color::GREEN, brightness);
  } else {
    board_.rgb1_set_color(hal::Color::RED, brightness);
  }
}

} // ns ioboard
} // ns acl