/**
 * @file hal.h
 * @brief Hardware abstraction layer (hal) to be implemented by specific PCBs
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#pragma once

#include <cstdint>
#include <cstddef>

namespace acl {
namespace ioboard {
namespace hal {

  /**
   * @brief      Available standard colors for RGB LED
   */
  enum class Color: uint8_t { OFF, RED, GREEN, BLUE, YELLOW, MAGENTA, CYAN, WHITE };

  class AbstractBoard
  {
  public:

    virtual void init() = 0;
    virtual void reset(bool bootloader) = 0;

    virtual uint32_t clock_millis() = 0;
    virtual uint32_t clock_micros() = 0;

    virtual void serial_write(const uint8_t *src, size_t len) = 0;
    virtual uint16_t serial_bytes_available() = 0;
    virtual uint8_t serial_read() = 0;
    virtual void serial_flush() = 0;

    virtual void esc_write(size_t channel, float value) = 0;

    virtual void rgb1_set_color(Color color, uint8_t brightness=100) = 0;
    virtual void rgb2_set_color(Color color, uint8_t brightness=100) = 0;
    
  };

} // ns hal
} // ns ioboard
} // ns acl
