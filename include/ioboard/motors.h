/**
 * @file motors.h
 * @brief Module for communicating with the motor ESCs
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#pragma once

#include <cstddef>

#include "ioboard/hal.h"

namespace acl {
namespace ioboard {

  /**
   * @brief      Forward declaration
   */
  class IOBoard;

  static constexpr size_t NUM_MOTORS = 6;

  /**
   * @brief      Manages esc communications
   */
  class Motors
  {
  public:
    Motors(IOBoard& ioboard);

    void set_new_motorcmd(const float throttle[6]);

    void write();
    void stop();
    

  private:
    IOBoard& ioboard_;
    hal::AbstractBoard& board_;

    bool has_new_cmd_ = false; ///< have recvd new cmd from flight computer?
    float throttle_[6]; ///< motor commands in pwm, 1000 to 2000 usec

  };

} // ns ioboard
} // ns acl