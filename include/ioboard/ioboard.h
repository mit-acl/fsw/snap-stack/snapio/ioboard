/**
 * @file ioboard.h
 * @brief Main ioboard execution
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#pragma once

#include "ioboard/hal.h"
#include "ioboard/comm_manager.h"
#include "ioboard/motors.h"

namespace acl {
namespace ioboard {

  class IOBoard
  {
  public:
    IOBoard(hal::AbstractBoard& board)
    : board_(board), comm_manager_(*this), motors_(*this) {}

    void run();
    
    hal::AbstractBoard& board_;

    /// \brief ioboard modules, mutually accessible via ioboard
    CommManager comm_manager_;
    Motors motors_;

  private:


  };

} // ns ioboard
} // ns acl