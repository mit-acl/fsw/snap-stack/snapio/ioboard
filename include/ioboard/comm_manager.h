/**
 * @file comm_manager.h
 * @brief Communication handling with flight computer
 * @author Parker Lusk <plusk@mit.edu>
 * @date 17 July 2021
 */

#pragma once

#include <cstddef>
#include <functional>

#include "ioboard/hal.h"
#include "snapio-protocol/sio_serial.h"

namespace acl {
namespace ioboard {

  /**
   * @brief      Forward declaration
   */
  class IOBoard;

  /**
   * @brief      Manages communications
   */
  class CommManager
  {
  public:
    CommManager(IOBoard& ioboard);

    void receive();
    void stream();
    bool is_stale();
    

  private:
    static constexpr int COMM_TIMEOUT_USEC = 500000; // safety stop on comm timeout

    IOBoard& ioboard_;
    hal::AbstractBoard& board_;

    uint32_t last_msg_time_us_;

    sio_serial_message_t in_buf_;

    /**
     * Rx functions
     */

    void handle_sio_message();
    void handle_msg_motorcmd(const sio_serial_motorcmd_msg_t& msg);

    /**
     * Tx functions
     */

    void send_heartbeat();

    /**
     * @brief      Each stream gets its own id
     */
    enum StreamId
    {
      STREAM_ID_HEARTBEAT,

      STREAM_COUNT // how many streams are there? (assumes 0-based indexing)
    };

    /**
     * @brief      Each stream manages sending a single message type at a specific rate
     */
    class Stream
    {
    public:
      Stream(uint32_t period_us, std::function<void(void)> send_function);

      void stream(uint32_t now_us);
      void set_rate_hz(uint32_t rate_hz);

      uint32_t period_us_;
      uint32_t next_time_us_;
      std::function<void(void)> send_function_;
    };

    Stream streams_[STREAM_COUNT] = {
      Stream(0, [this] { this->send_heartbeat(); }),
    };

  };

} // ns ioboard
} // ns acl